'use strict'

const menuTabs = document.querySelectorAll('.tabs-title');
menuTabs.forEach((element) => {
    element.addEventListener("click", (e) => {
        const allTabs = document.querySelectorAll('.tabs-title');
        allTabs.forEach((element) => {
            element.classList.remove('active');
        })
        e.target.classList.add('active');

        const tabsContent = document.querySelectorAll('.tabs-content li');
        tabsContent.forEach((element) => {
            element.style.display = 'none';
        })
        const dataAttribute = e.target.getAttribute('data-tab');
        const foundEl = document.querySelector(`.tabs-content [data-tab="${dataAttribute}"]`);
        foundEl.style.display = 'list-item';
    })
})
